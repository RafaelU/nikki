package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

public class BitacoraAudio extends Bitacora {


    public BitacoraAudio(int id, String path, String fecha) {
        super(id, path, fecha);
    }

    public BitacoraAudio(int identificador,String titulo_bitacora, String path, String fecha_bitacora,int emocion){
        super(identificador,titulo_bitacora, path, fecha_bitacora,emocion);
    }

    public String getPath() {
        return info;
    }

    public void setPath(String path) {
        this.info = path;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmocion() {
        return emocion;
    }

    @NonNull
    @Override
    public String toString() {
        return "BitacoraAudio_"+ id + " " + fecha;
    }
}
