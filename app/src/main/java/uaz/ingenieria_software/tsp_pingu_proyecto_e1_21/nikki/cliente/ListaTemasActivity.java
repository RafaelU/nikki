package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.databinding.ActivityListaTemasBinding;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.ListAdapter;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Tema;

public class ListaTemasActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private NavigationView menu;
    private manejadorMenu mm;
    private TextView tituloTema;
    private ImageView imagen;
    private ListView lista;
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;
    ActivityListaTemasBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Se crea la instancia de la actividad.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_temas);

        binding = ActivityListaTemasBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Se crea una instancia de manejadorMenu y se asocia al menú.
        mm = new manejadorMenu(ListaTemasActivity.this);
        menu = findViewById(R.id.navViewListaTemas);
        menu.setNavigationItemSelectedListener(mm);

        drawerLayout = findViewById(R.id.DrawerListTem);
        btn_menu = findViewById(R.id.btn_menu_listaTemas);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        int[] img = {R.drawable.imagen, R.drawable.imagen4, R.drawable.imagen3, R.drawable.imagen2};
        String[] titulo = {"¿Qué es la Ansiedad Generalizada?", "Factores de riesgo", "Síntomas" , "Causas"};
        String[] contenido = {"Es un trastorno mental en el cual una persona a menudo está preocupada o ansiosa respecto a " +
                "muchas cosas y le parece difícil controlar esta ansiedad." ,
                "Personalidad. Una persona que es tímida o que tiene un temperamento negativo o que evita" +
                " cualquier situación peligrosa puede ser más propensa a padecer un trastorno de ansiedad generalizada que otras.",
                "Preocupación o ansiedad persistentes por determinados asuntos que son desproporcionados en relación con el impacto de los acontecimientos \n" +
                        "Pensar demasiado los planes y las soluciones a todos los peores resultados posibles \n " +
                        "Incapacidad para relajarse, sensación de nerviosismo y sensación de excitación o de estar al límite",
                "Como sucede con muchas enfermedades mentales, la causa del trastorno de ansiedad generalizada probablemente surge de una interacción compleja de factores biológicos y ambientales."};

        dbHelper = new DBHelper(getApplicationContext());
        lista = findViewById(R.id.lista_temas);

        ArrayList<Tema> temaArrayList = new ArrayList<>();
        for(int i = 0 ; i < img.length ; i++){
            Tema tema = new Tema(titulo[i],contenido[i],img[i]);
            temaArrayList.add(tema);
        }

        ListAdapter listAdapter = new ListAdapter(ListaTemasActivity.this, temaArrayList);

        binding.listaTemas.setAdapter(listAdapter);
        binding.listaTemas.setClickable(true);
        binding.listaTemas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(ListaTemasActivity.this, TemaActivity.class);
                intent.putExtra("titulo", titulo[position]);
                intent.putExtra("contenido", contenido[position]);
                intent.putExtra("imagen", img[position]);
                startActivity(intent);
            }
        });
    }
}
