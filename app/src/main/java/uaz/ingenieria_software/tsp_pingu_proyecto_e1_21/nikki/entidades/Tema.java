package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

public class Tema {

    String titulo, contenido;
    int imagenId;

    public Tema(String titulo, String contenido, int imagenId) {
        this.titulo = titulo;
        this.contenido = contenido;
        this.imagenId = imagenId;
    }

    public Tema(){}

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public int getImagenId() {
        return imagenId;
    }

    public void setImagenId(int imagenId) {
        this.imagenId = imagenId;
    }

}
