package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;

public class RecycledViewAdaptador extends RecyclerView.Adapter<RecycledViewAdaptador.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView titulo, fecha;
        private ImageView tipo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titulo=(TextView)itemView.findViewById(R.id.bitacora_titulo_item);
            fecha=(TextView)itemView.findViewById(R.id.fecha_bitacora_item);
            tipo=(ImageView) itemView.findViewById(R.id.image_bitacora_cardview);
        }
    }
    public List<Bitacora> bitacorasLista;

    public RecycledViewAdaptador(List<Bitacora> lista){
        this.bitacorasLista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bitacora,parent,false);
        ViewHolder viewholder = new ViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.titulo.setText(String.valueOf(bitacorasLista.get(position).getTitulo()));
        holder.fecha.setText(bitacorasLista.get(position).getFecha());
        if (bitacorasLista.get(position) instanceof BitacoraTexto){
            holder.tipo.setImageResource(R.drawable.ic_pen_circle_foreground);
        }
        else {
            holder.tipo.setImageResource(R.drawable.ic_mic_circle_foreground);
        }
    }

    @Override
    public int getItemCount() {
        return bitacorasLista.size();
    }
}
