package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBTemas;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;

public class MainActivity extends AppCompatActivity {

    // Atributos necesarios.
    private NavigationView menu;
    private Button btn_regNotaVoz;
    private View btn_bitacora_texto;
    private manejadorMenu mm;
    final int REQUEST_CODE = 200;

    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Se crea la instancia de la actividad.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Verifica permisos 
        verificarPermisos();

        // Activa los recordatorios
        miRecordatorio();

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        int nightModeFlags = MainActivity.this.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        switch (nightModeFlags) {
            case Configuration.UI_MODE_NIGHT_YES:
                /* si esta activo el modo oscuro lo desactiva */
                AppCompatDelegate.setDefaultNightMode(
                        AppCompatDelegate.MODE_NIGHT_NO);
                break;
        }
        //copia los recursos de los temas
        iniciarTemas();

        // Se crea una instancia de manejadorMenu y se asocia al menú.
        mm = new manejadorMenu(MainActivity.this);
        menu = findViewById(R.id.navViewInico);
        menu.setNavigationItemSelectedListener(mm);

        drawerLayout = findViewById(R.id.DrawerNavViewInico);
        btn_menu = findViewById(R.id.btn_menu_main);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START); //Permite arrastrar el menu lateral
            }
        });

        // Se busca el activity BitacoraTextoActivity para asociarla con el botón.
        btn_bitacora_texto = findViewById(R.id.btn_bitacora_texto);
        btn_regNotaVoz = findViewById(R.id.btn_bitacora_audio);

        // Se crea una instancia del manejador del botón para crear una bitácora de texto.
        manejadorBtnBitacoraTexto mbt = new manejadorBtnBitacoraTexto();
        btn_bitacora_texto.setOnClickListener(mbt);

        //Se agregan los click listeners de los botones
        btn_regNotaVoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        MainActivity.this,
                        BitacoraAudioActivity.class
                );
                startActivity(intent);
            }
        });

    }


    // Clase que se encaraga de manejar el funcionamiento del botón para crear una bitácora de texto.
    private class manejadorBtnBitacoraTexto implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, BitacoraTextoActivity.class);
            startActivity(intent);
        }
    }

    // Método encargado de la verificación y solicitud de los permisos de la aplicación.
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void verificarPermisos(){
        int permisosMic=ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int permisosAmlacenamientoEsc=ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permisosAmlacenamientoLec=ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permisosMic == PackageManager.PERMISSION_DENIED && permisosAmlacenamientoEsc == PackageManager.PERMISSION_DENIED && permisosAmlacenamientoLec == PackageManager.PERMISSION_DENIED){
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE);
        }
    }

    private void iniciarTemas(){
        SharedPreferences wmbPreference = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun) { //isFirstRun
            File path;
            path = new File(getExternalFilesDir(null)+"/recursosTemas");
            String[] temImg = DBTemas.recursosImg;
            for (int ind = 0; ind < temImg.length; ind++){
                copiarRecursos(path,temImg[ind]);
            }


            SharedPreferences.Editor editor = wmbPreference.edit();
            editor.putBoolean("FIRSTRUN", false);
            editor.apply();
        }
    }

    private void copiarRecursos(File pathDestino,String recurso){
        if (pathDestino.exists() == false){
            pathDestino.mkdir();
        }
        pathDestino.mkdirs();
        AssetManager assetManager = getAssets();
        InputStream in = null;
        OutputStream out = null;
        try{
            in = assetManager.open(recurso);
            File outFile = new File(pathDestino,recurso);
            out = new FileOutputStream(outFile);
            copiarArchivo(in, out);
            Toast.makeText(this, "recursos cargados", Toast.LENGTH_SHORT).show();
        }catch (IOException e){
            e.printStackTrace();
            Toast.makeText(this, "fallo la carga de los recursos", Toast.LENGTH_SHORT).show();
        }finally {
            if (in != null){
                try {
                    in.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (out != null){
                try {
                    out.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void copiarArchivo(InputStream in, OutputStream out) throws IOException{
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }


    /**
     * Método que se encarga de mostrar notificaciones/recordatorios al usuario para registrar sus bitácoras
     */
    public void miRecordatorio(){
        //Manejar de alarmas
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //Intent que manda llamar a la clase Recordatorio Broadcast, que es la encargada de manejar la alarma
        Intent intent = new Intent(getApplicationContext(), RecordatorioBroadcast.class);

        //Primer recordatorio
        Calendar recordatorio1 = Calendar.getInstance();
        recordatorio1.set(Calendar.HOUR_OF_DAY, 15);
        recordatorio1.set(Calendar.MINUTE, 36);
        recordatorio1.set(Calendar.SECOND, 00);
        PendingIntent recordatorioIntent1 = PendingIntent.getBroadcast(getApplicationContext(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Segundo recordatorio
        Calendar recordatorio2 = Calendar.getInstance();
        recordatorio2.set(Calendar.HOUR_OF_DAY, 21);
        recordatorio2.set(Calendar.MINUTE, 55);
        recordatorio2.set(Calendar.SECOND, 00);
        PendingIntent recordatorioIntent2 = PendingIntent.getBroadcast(getApplicationContext(), 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (recordatorio1.getTime().compareTo(new Date()) < 0) {
            recordatorio1.add(Calendar.DAY_OF_MONTH, 1);
        }

        if (recordatorio2.getTime().compareTo(new Date()) < 0) {
            recordatorio2.add(Calendar.DAY_OF_MONTH, 1);
        }

        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, recordatorio1.getTimeInMillis(), AlarmManager.INTERVAL_DAY, recordatorioIntent1);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, recordatorio2.getTimeInMillis(), AlarmManager.INTERVAL_DAY, recordatorioIntent2);
        }
    }
}