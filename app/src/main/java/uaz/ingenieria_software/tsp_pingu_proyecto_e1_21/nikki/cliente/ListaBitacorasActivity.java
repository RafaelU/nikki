package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Bitacora;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraAudio;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraTexto;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.RecycledViewAdaptador;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.RecyclerItemClickListener;

public class ListaBitacorasActivity extends AppCompatActivity {

    private View btn_atras; // Botón atrás.
    private NavigationView menu; // Vista correspondiente al menú.
    private manejadorMenu mm; // Manejador del menú.
    private RecyclerView lista_bitacoras; // Vista de la lista de bitácoras.
    private RecycledViewAdaptador adaptador;
    private List<Bitacora> bitacoras; // Lista donde se almacenan las bitácoras.
    private DBHelper base_datos; // Manejador de la base de datos.
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;
    public static final int DELETE_CODE=104; // Código de eliminación.
    public static final int DETAIL_CODE=105; // Código de detalle.

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // Se crea la instancia de la actividad.
        setContentView(R.layout.activity_lista_bitacoras); // Se le asocia un layout.

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mm = new manejadorMenu(ListaBitacorasActivity.this); // Se inicializa el manejador del menú.
        menu = findViewById(R.id.navViewListaBitacoras); // Se obtiene el menú por su id.
        menu.setNavigationItemSelectedListener(mm); // Se le asocia al menú su manejador.

        drawerLayout = findViewById(R.id.DrawerListBit);
        btn_menu = findViewById(R.id.btn_menu_ListBit);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        base_datos = new DBHelper(getApplicationContext()); // Se inicializa el manejador de la base de datos.
        bitacoras = base_datos.obtenerBitacoras(); // Se obtienen las bitácoras de una correspondiente fecha.

        Collections.sort(bitacoras, new comparadorBitacorasporFecha());

        lista_bitacoras = (RecyclerView) findViewById(R.id.recycler_bitacora); // Se obtiene la lista de bitácoras por su id.
        lista_bitacoras.setLayoutManager(new LinearLayoutManager(this));

        adaptador = new RecycledViewAdaptador(bitacoras);
        lista_bitacoras.setAdapter(adaptador);

        lista_bitacoras.addOnItemTouchListener(
                new RecyclerItemClickListener(this, lista_bitacoras ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        Bitacora bitacora_seleccionada = (Bitacora)bitacoras.get(position); // Se obtiene la posición del item que se selecciono en la lista.
                        if (bitacora_seleccionada instanceof BitacoraAudio){// Si la bitacora es de audio.
                            Intent intent = new Intent(ListaBitacorasActivity.this, DetalleBitacoraAudioActivity.class); // Se inicializa un nuevo intent que lanzará DetalleBitacoraAudioActivity.
                            //BitacoraAudio bitacora_audio = base_datos.obtenerBitacoraAudio(bitacora_seleccionada.getId()); // Se obtiene la bitácora específica de la base de datos.
                            Log.e("BitacoraselecAud",""+bitacora_seleccionada);
                            intent.putExtra("info", bitacora_seleccionada); // Se añade al itent para ser usado por la DetalleBitacoraAudioActivity.
                            startActivityForResult(intent,DETAIL_CODE); // Se lanza el intent.
                        }
                        else{
                            Intent intent = new Intent(ListaBitacorasActivity.this, DetalleBitacoraTextoActivity.class);// Se inicializa un nuevo intent que lanzará DetalleBitacoraTextoActivity.
                            //BitacoraTexto bitacora_texto = base_datos.obtenerBitacoraTexto(bitacora_seleccionada.getId()); // Se obtiene la bitácora específica de la base de datos.
                            Log.e("BitacoraselecTxt",""+bitacora_seleccionada);
                            intent.putExtra("info", bitacora_seleccionada); // Se añade al itent para ser usado por la DetalleBitacoraTextoActivity.
                            startActivityForResult(intent,DETAIL_CODE); // Se lanza el intent.
                        }

                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        btn_atras = findViewById(R.id.btn_atras_lista_bitacoras);
        manejadorAtrasListaBitacoras manejador_btn_atras_lista = new manejadorAtrasListaBitacoras();
        btn_atras.setOnClickListener(manejador_btn_atras_lista);

    }
    private class manejadorAtrasListaBitacoras implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            regresar(v);
        }
    }

    //Método para terminar la activity y regresar
    public void regresar(View v){
        finish();
    }

    @Override
    public void finish() {
        Intent intent_registros = new Intent(ListaBitacorasActivity.this, MainActivity.class);
        startActivity(intent_registros);
        super.finish();
    }
    private class comparadorBitacorasporFecha implements Comparator<Bitacora> {
        @Override
        public int compare(Bitacora bitacora, Bitacora bitacora2) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date fecha1 = df.parse(bitacora.getFecha());
                Date fecha2 = df.parse(bitacora2.getFecha());

                return fecha1.compareTo(fecha2);
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }


        }
    }

    /*
    private class manejadorlistaBitacoras implements RecyclerView.OnItemTouchListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Bitacora bitacora_seleccionada = (Bitacora)parent.getItemAtPosition(position); // Se obtiene la posición del item que se selecciono en la lista.
            if (bitacora_seleccionada instanceof BitacoraAudio){// Si la bitacora es de audio.
                Intent intent = new Intent(ListaBitacorasActivity.this, DetalleBitacoraAudioActivity.class); // Se inicializa un nuevo intent que lanzará DetalleBitacoraAudioActivity.
                //BitacoraAudio bitacora_audio = base_datos.obtenerBitacoraAudio(bitacora_seleccionada.getId()); // Se obtiene la bitácora específica de la base de datos.
                Log.e("BitacoraselecAud",""+bitacora_seleccionada);
                intent.putExtra("info", bitacora_seleccionada); // Se añade al itent para ser usado por la DetalleBitacoraAudioActivity.
                startActivityForResult(intent,DETAIL_CODE); // Se lanza el intent.
            }
            else{
                Intent intent = new Intent(ListaBitacorasActivity.this, DetalleBitacoraTextoActivity.class);// Se inicializa un nuevo intent que lanzará DetalleBitacoraTextoActivity.
                //BitacoraTexto bitacora_texto = base_datos.obtenerBitacoraTexto(bitacora_seleccionada.getId()); // Se obtiene la bitácora específica de la base de datos.
                Log.e("BitacoraselecTxt",""+bitacora_seleccionada);
                intent.putExtra("info", bitacora_seleccionada); // Se añade al itent para ser usado por la DetalleBitacoraTextoActivity.
                startActivityForResult(intent,DETAIL_CODE); // Se lanza el intent.
            }
        }

    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == DELETE_CODE && requestCode == DETAIL_CODE) {
            if (data.hasExtra("bitacora")) {
                Bitacora bitacora_eliminada = (Bitacora) data.getSerializableExtra("bitacora");
                Log.e("BitacoraEliminada",""+bitacora_eliminada);
                bitacoras.remove(bitacora_eliminada);
                adaptador.notifyDataSetChanged();
            }
        }
    }
}
