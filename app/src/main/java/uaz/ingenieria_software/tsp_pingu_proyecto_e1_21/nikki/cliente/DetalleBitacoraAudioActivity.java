package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AndroidException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Bitacora;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraAudio;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Emocion;

public class DetalleBitacoraAudioActivity extends AppCompatActivity {

    private NavigationView menu;
    private manejadorMenu mm;
    private View btn_atrasListas;
    private View btn_eliminarBitacoraAud;
    private TextView txt_fecha_bitacora;
    private TextView txt_duracion_aud;
    private TextView tituloView;
    private TextView emocionView;
    private MaterialButton btn_reproduccion;
    private SeekBar barra_prog;
    private DBHelper dbhelper;
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;

    private Handler handler = new Handler();
    private MediaMetadataRetriever metadataRetriever;
    private MediaPlayer mediaPlayer;
    Runnable runnable;
    long totalMillSecond;
    long currentMillSecond;
    long totalMinutes;
    long totalSeconds;
    private boolean reproduciendo;

    BitacoraAudio bitacora;

    protected void onCreate(Bundle savedInstanceState) {
        //Se crea la instancia de la actividad.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_bitacora_aud);
        dbhelper = new DBHelper(getApplicationContext());

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mm = new manejadorMenu(DetalleBitacoraAudioActivity.this);
        menu = findViewById(R.id.navViewDetalleBitacoraAudio);
        menu.setNavigationItemSelectedListener(mm);

        drawerLayout = findViewById(R.id.DrawerDetBitAud);
        btn_menu = findViewById(R.id.btn_menu_DetBitAud);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        // Se busca el botón atras de activity_bitacora_texto.xml.
        // Se crea una instancia de la clase manejadorAtrasBtnBitAudio y se le asigna a el botón.
        btn_atrasListas = findViewById(R.id.btn_Atras_DetalleBit_Aud);
        manejadorAtrasBtnListaBitacorasAudio mabt = new manejadorAtrasBtnListaBitacorasAudio();
        btn_atrasListas.setOnClickListener(mabt);

        btn_eliminarBitacoraAud = findViewById(R.id.btn_eliminar_bitacora_aud);
        manejadorEliminarBtnBitacoraAudio mebt = new manejadorEliminarBtnBitacoraAudio();
        btn_eliminarBitacoraAud.setOnClickListener(mebt);

        //bincular botones del reproductor e iniciar sus manejadores de evento
        txt_fecha_bitacora = findViewById(R.id.txt_fecha_bitacora);
        btn_reproduccion = findViewById(R.id.btn_play_bitacora_aud);
        txt_duracion_aud = findViewById(R.id.txt_duracion_aud);
        barra_prog = findViewById(R.id.barra_progreso_aud);
        tituloView = findViewById(R.id.textViewTituloDetalleBitacoraAud);
        emocionView = findViewById(R.id.textViewEmocionAud);


        //carga la bitacora
        if(savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if (extras == null ){
                bitacora = null;
            }else {
                bitacora = (BitacoraAudio) extras.getSerializable("info");
                txt_fecha_bitacora.setText(bitacora.getFecha());
            }
        }else {
            bitacora = (BitacoraAudio) savedInstanceState.getSerializable("info");
            txt_fecha_bitacora.setText(bitacora.getFecha());
        }

        if (bitacora == null){
            Log.e("detBitacoraAudio","No se recibió Objeto Bitácora");
            Toast.makeText(this, "No se recibió Objeto Bitácora", Toast.LENGTH_SHORT).show();
            finish();
        }else if (!(new File(bitacora.getPath())).exists()){
            Log.e("detBitacoraAudio","No existe el archivo de audio");
            Toast.makeText(this, "No existe el archivo de audio", Toast.LENGTH_SHORT).show();
            finish();
        }

        Log.d("detBitacoraAudio",bitacora+" "+bitacora.getPath());
        metadataRetriever = new MediaMetadataRetriever();
        metadataRetriever.setDataSource(bitacora.getPath());
        totalMillSecond = Long.parseLong(metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        metadataRetriever.close();
        metadataRetriever = null;
        totalMinutes = TimeUnit.MILLISECONDS.toMinutes(totalMillSecond);
        totalSeconds = TimeUnit.MILLISECONDS.toSeconds(totalMillSecond - totalMinutes);

        //iniciar reproductor de musica
        mediaPlayer = MediaPlayer.create(this, Uri.fromFile(new File(bitacora.getPath())));
        runnable = new Runnable() {
            @Override
            public void run() {
                //asignar el progreso en la barra
                barra_prog.setProgress(mediaPlayer.getCurrentPosition());
                asignartxtDuracion(mediaPlayer.getCurrentPosition());
                handler.postDelayed(this,500);
            }
        };

        txt_fecha_bitacora.setText(bitacora.getFecha());
        Emocion e = dbhelper.obtenerEmocion(bitacora.getEmocion());
        tituloView.setText(bitacora.getTitulo());
        emocionView.setText("Emoción: "+e.getNombre());

        asignartxtDuracion(0);

        reproduciendo = false;
        btn_reproduccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (reproduciendo == false){ //si no esta reproduciendo
                    //cambiar reproductor a estado reproduciendo
                    reproduciendo = !reproduciendo;
                    btn_reproduccion.setIcon(getDrawable(android.R.drawable.ic_media_pause));
                    btn_menu.setVisibility(View.GONE);
                    menu.setNavigationItemSelectedListener(null);

                    //iniciar
                    mediaPlayer.start();
                    barra_prog.setMax(mediaPlayer.getDuration());
                    handler.postDelayed(runnable,0);

                } else { //estaba reproduciendo
                    //cambiar a modo no reproducir
                    reproduciendo = !reproduciendo;
                    btn_reproduccion.setIcon(getDrawable(android.R.drawable.ic_media_play));
                    btn_menu.setVisibility(View.VISIBLE);
                    menu.setNavigationItemSelectedListener(mm);

                    //pausar
                    mediaPlayer.pause();
                    //parar handler
                    handler.removeCallbacks(runnable);

                }
            }
        });

        barra_prog.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //verificar condicion
                if(b){ //cuando el usuario  fue quien la movio poner el progreso en la barra
                    mediaPlayer.seekTo(i);
                }
                //actualizar el texto de duracion
                asignartxtDuracion(mediaPlayer.getCurrentPosition());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) { //cuando la reproduccion se complete
                //cambiar estado a no reproduciendo
                reproduciendo = !reproduciendo;
                btn_reproduccion.setIcon(getDrawable(android.R.drawable.ic_media_play));
                //volver reproductor a el estado inicial
                mediaPlayer.seekTo(0);
            }
        });

    }

    private void asignartxtDuracion(int mCurrentPosition){
        long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition - minutes);
        //long minutes = TimeUnit.MICROSECONDS.toMinutes(mCurrentPosition);
        //long seconds = TimeUnit.MICROSECONDS.toSeconds(mCurrentPosition - TimeUnit.MINUTES.toSeconds(minutes));
        txt_duracion_aud.setText(String.format("%02d:%02d / %02d:%02d",minutes,seconds,totalMinutes,totalSeconds));
    }




    // Clase perteneciente a la funcionalidad del botón atrás
    private class manejadorAtrasBtnListaBitacorasAudio implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            regresar(v);
        }
    }

    //Método para terminar la activity y regresar
    public void regresar(View v) {
        finish();
    }

    @Override
    public void finish() {

        saliendo();
        super.finish();
    }

    private void saliendo(){
        if(reproduciendo == true){
            mediaPlayer.stop();
        }
    }

    @Override
    protected void onPause() {
        saliendo();
        super.onPause();
    }

    @Override
    protected void onStop() {
        saliendo();
        super.onStop();
    }

    @Override
    protected void onUserLeaveHint() {
        saliendo();
        super.onUserLeaveHint();
    }

    @Override
    public void onBackPressed() {
        saliendo();
        super.onBackPressed();
    }



    private class manejadorEliminarBtnBitacoraAudio implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            mostrarConfirmDialog();
        }
    }

    private void mostrarConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetalleBitacoraAudioActivity.this);
        View v = LayoutInflater.from(DetalleBitacoraAudioActivity.this).inflate(
                R.layout.confirm_dialog,
                (ConstraintLayout)findViewById(R.id.layoutDialogContainer)
        );
        builder.setView(v);
        AlertDialog alertDialog = builder.create();
        //Aqui vamos a eliminar el registro (conforme su id)
        v.findViewById(R.id.Confirm_dialog_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File archivo = new File(bitacora.getPath());
                if (dbhelper.eliminarBitacoraAUD(bitacora.getId()) && archivo.delete()) {
                    Intent data = new Intent();
                    data.putExtra("bitacora", bitacora);
                    // Activity finished ok, return the data
                    setResult(ListaBitacorasActivity.DELETE_CODE, data);

                    Toast.makeText(getApplicationContext(),"bitacora eliminada con éxito", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                    finish();
                }else {
                    alertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"No se pudo eliminar Bitacora", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Aqui vamos a abortar la eliminacion ("mejor no" opcion)
        v.findViewById(R.id.Confirm_dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        if(alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
        alertDialog.show();
    }

    //Este método hace que nos regresemos a la interfaz de lista de bitácoras cuando el registro es eliminado
    private void lista(){
        Intent intent = new Intent(this, ListaBitacorasActivity.class);
        startActivity(intent);
    }
}
