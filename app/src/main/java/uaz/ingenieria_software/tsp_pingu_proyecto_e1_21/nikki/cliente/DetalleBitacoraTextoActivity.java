package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import androidx.appcompat.app.*;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraAudio;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraTexto;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Emocion;

public class DetalleBitacoraTextoActivity extends AppCompatActivity {

    private NavigationView menu;
    private manejadorMenu mm;
    private View btn_atrasListas;
    private View btn_eliminarBitacoraAud;
    private TextView detalle_registro;
    private TextView fechaView;
    private TextView tituloView;
    private TextView infoView;
    private TextView emocionView;
    private DBHelper dbhelper;
    private BitacoraTexto bitacora;
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;

    int id = 0;

    protected void onCreate(Bundle savedInstanceState) {
        //Se crea la instancia de la actividad.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_bitacora_txt);
        dbhelper = new DBHelper(getApplicationContext());

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mm = new manejadorMenu(DetalleBitacoraTextoActivity.this);
        menu = findViewById(R.id.navViewDetalleBitacoraTexto);
        menu.setNavigationItemSelectedListener(mm);

        drawerLayout = findViewById(R.id.DrawerDetBitTxt);
        btn_menu = findViewById(R.id.btn_menu_DetBitTxt);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        // Se busca el botón atras de activity_bitacora_texto.xml.
        // Se crea una instancia de la clase manejadorAtrasBtnBitAudio y se le asigna a el botón.
        btn_atrasListas = findViewById(R.id.btn_Atras_DetalleBit_txt2);
        manejadorAtrasBtnListaBitacorasAudio mabt = new manejadorAtrasBtnListaBitacorasAudio();
        btn_atrasListas.setOnClickListener(mabt);

        btn_eliminarBitacoraAud = findViewById(R.id.btn_eliminar_bitacora_txt);
        manejadorEliminarBtnBitacoraAudio mebt = new manejadorEliminarBtnBitacoraAudio();
        btn_eliminarBitacoraAud.setOnClickListener(mebt);

        //inicializa las variables que se desean mostrar, desde el intent
        infoView = findViewById(R.id.txt_bitacora_txt);
        fechaView = findViewById(R.id.txt_fecha_bitacora2);
        tituloView = findViewById(R.id.textViewTituloDetalleBitacoraTexto);
        emocionView = findViewById(R.id.textViewEmocion);
        detalle_registro = findViewById(R.id.txt_detalle_registro2);

        //extra la info necesaria de la instancia bitacora, la cual extra una bitacora del intent
        if(savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if (extras == null ){
                bitacora = null;
            }else {
                bitacora = (BitacoraTexto) extras.getSerializable("info");
            }
        }else {
            bitacora = (BitacoraTexto) savedInstanceState.getSerializable("info");
        }

        if (bitacora == null) {
            Toast.makeText(this, "No existe la bitácora", Toast.LENGTH_SHORT).show();
        } else {
            String bitacora_texto = bitacora.getBitacora();
            String fecha = bitacora.getFecha();
            tituloView.setText(bitacora.getTitulo());
            infoView.setText(bitacora_texto);
            Emocion e = dbhelper.obtenerEmocion(bitacora.getEmocion());
            emocionView.setText("Emoción: "+e.getNombre());
            fechaView.setText(fecha);
        }
    }

    // Clase perteneciente a la funcionalidad del botón atrás
    private class manejadorAtrasBtnListaBitacorasAudio implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            regresar(v);
        }
    }

    //Método para terminar la activity y regresar
    public void regresar(View v) {
        finish();
    }

    @Override
    public void finish() {

        super.finish();
    }

    private class manejadorEliminarBtnBitacoraAudio implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            mostrarConfirmDialog();
        }
    }

    private void mostrarConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetalleBitacoraTextoActivity.this);
        View v = LayoutInflater.from(DetalleBitacoraTextoActivity.this).inflate(
                R.layout.confirm_dialog,
                (ConstraintLayout)findViewById(R.id.layoutDialogContainer)
        );
        builder.setView(v);
        AlertDialog alertDialog = builder.create();
        //Aqui vamos a eliminar el registro (conforme su id)
        v.findViewById(R.id.Confirm_dialog_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dbhelper.eliminarBitacoraTXT(bitacora.getId())) {
                    Intent data = new Intent();
                    data.putExtra("bitacora", bitacora);
                    setResult(ListaBitacorasActivity.DELETE_CODE, data);
                    Toast.makeText(getApplicationContext(),"Bitacora eliminada con éxito", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                    finish();
                }else {
                    alertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"No se pudo eliminar Bitacora", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Aqui vamos a abortar la eliminacion ("mejor no" opcion)
        v.findViewById(R.id.Confirm_dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        if(alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
        alertDialog.show();
    }

    //Este método hace que nos regresemos a la interfaz de lista de bitácoras cuando el registro es eliminado
    private void lista(){
        Intent intent = new Intent(this, ListaBitacorasActivity.class);
        startActivity(intent);
    }
}
