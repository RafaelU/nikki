package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraAudio;

public class Grabador extends Service {
    MediaRecorder mediaRecorder;
    File path;
    long tiempoInicio = 0;
    long tiempoPasado = 0;
    Intent intent;

    String nombre;

    DBHelper dbHelper;


    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper = new DBHelper(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.intent = intent;
        comenzarGrabacion();
        return START_STICKY;
    }

    private void comenzarGrabacion(){
        nombre = (System.currentTimeMillis()/1000)+"";
        nombre = "bitacora_"+nombre;
        path = new File(getExternalFilesDir(null)+"/sonido/"+nombre+".mp3");

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setAudioEncodingBitRate(16);
        mediaRecorder.setAudioSamplingRate(44100);
        mediaRecorder.setOutputFile(path.getAbsolutePath());

        try{
            mediaRecorder.prepare();
            mediaRecorder.start();
            tiempoInicio = System.currentTimeMillis();
        }catch (IOException e){
            Log.e("Grabar", e.toString());
        }

    }

    private void pararGrabacion(){
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
        tiempoPasado = System.currentTimeMillis() - tiempoInicio;

        String toas = ""+getResources().getString(R.string.grabacion_guardada)+" "+path.getPath();
        Toast.makeText(getApplicationContext(), toas , Toast.LENGTH_LONG).show();


        Bundle extras = intent.getExtras();
        String titulo = (String)extras.get("titulo");

        long ahora = System.currentTimeMillis();
        Date fecha = new Date(ahora);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String FechaSalida = df.format(fecha);
        BitacoraAudio bitacora = new BitacoraAudio(-1,titulo,path.getAbsolutePath(),FechaSalida,(int)extras.get("idEmocion"));
        dbHelper.añadirNuevaBitacoraAUD(bitacora);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (mediaRecorder!= null){
            pararGrabacion();
        }
        super.onDestroy();
    }
}
