package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.navigation.NavigationView;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.databinding.ActivityDetalleTemaBinding;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.databinding.ActivityListaTemasBinding;

public class TemaActivity extends AppCompatActivity {

    private NavigationView menu;
    private View btn_atras;
    private DBHelper dbHelper;
    private ImageView imagen;
    private TextView titulo;
    private TextView contenido;
    private ActivityDetalleTemaBinding binding;
    private manejadorMenu mm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Se crea la instancia de la actividad.
        super.onCreate(savedInstanceState);
        binding = ActivityDetalleTemaBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent intent = this.getIntent();

        if(intent != null){
            String titulo = intent.getStringExtra("titulo");
            String contenido = intent.getStringExtra("contenido");
            int imagen = intent.getIntExtra("imagen", R.drawable.imagen);

            binding.temaImagen.setImageResource(imagen);
            binding.temaName.setText(titulo);
            binding.textTxt.setText(contenido);
        }

        // Se crea una instancia de manejadorMenu y se asocia al menú.
        mm = new manejadorMenu(TemaActivity.this);
        menu = findViewById(R.id.navViewDetalleTema);
        menu.setNavigationItemSelectedListener(mm);

        // Se busca el botón atras de activity_lista_temas.xml.
        // Se crea una instancia de la clase manejadorAtrasBtnBitacoraTexto y se le asigna a el botón.
        btn_atras = findViewById(R.id.tema_btn_atras);
        manejadorAtrasBtnListaTemas mablt = new manejadorAtrasBtnListaTemas();
        btn_atras.setOnClickListener(mablt);


    }

    // Clase perteneciente a la funcionalidad del botón atrás
    private class manejadorAtrasBtnListaTemas implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            regresar(v);
        }
    }

    //Método para terminar la activity y regresar
    public void regresar(View v){
        finish();
    }

    @Override
    public void finish() {

        super.finish();
    }



}
