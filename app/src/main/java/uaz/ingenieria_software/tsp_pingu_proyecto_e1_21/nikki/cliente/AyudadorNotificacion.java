package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;

/**
 * Clase que crea las notificaciones
 */
public class AyudadorNotificacion {


    private Context mContext;
    private static final String NOTIFICATION_CHANNEL_ID = "10001";
    public static final int NOTIFICACION_TARDE = 0;
    public static final int NOTIFICACION_NOCHE = 1;

    AyudadorNotificacion(Context context) {
        mContext = context;
    }

    /**
     * Metodo que crea la notificacion y la deja lista
     */
    void crearNotificacion(int id_notif)
    {

        Intent intent = new Intent(mContext , MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.drawable.imagen0); //se pone el icono de la aaplicacion
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(false);
        if       (id_notif == NOTIFICACION_TARDE){
            NotificacionTarde(mBuilder);
        }else if (id_notif == NOTIFICACION_NOCHE){
            NotificacionNoche(mBuilder);
        }


        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }

    /**
     * Construye la notificacion con el texto de la tarde
     * @param mBuilder
     */
    private void NotificacionTarde(NotificationCompat.Builder mBuilder){
        mBuilder.setContentTitle(mContext.getString(R.string.notif_titulo_tarde)); //titulo
        mBuilder.setContentText(mContext.getString(R.string.notif_desc_tarde)); //desc
    }

    /**
     * Construye la notificacion con el texto de la noche
     * @param mBuilder
     */
    private void NotificacionNoche(NotificationCompat.Builder mBuilder){
        mBuilder.setContentTitle(mContext.getString(R.string.notif_titulo_noche)); //titulo
        mBuilder.setContentText(mContext.getString(R.string.notif_desc_tarde)); //desc
    }
}
