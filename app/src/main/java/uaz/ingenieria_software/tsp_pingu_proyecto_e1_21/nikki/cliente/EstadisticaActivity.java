package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Emocion;

public class EstadisticaActivity extends AppCompatActivity {

    private NavigationView menu;
    private manejadorMenu mm;
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;
    private SimpleDateFormat formatoFecha; //formato de las fechas
    private DBHelper base_datos; // Manejador de la base de datos.
    private TextView nombreEmocionView;
    private View btn_ListaTemas;
    private int[] imagenes;
    private ImageView imagenEmocion; //imagen que se muestra según la emoción
    private String fecha0; // Fecha del día que se solicitan las estadísticas, la actual.
    private String fecha1;  // aquí se van a almacenar las fechas de los días de la semana anteriores al actual
    private String fecha2;
    private String fecha3;
    private String fecha4;
    private String fecha5;
    private String fecha6;
    private String fecha7;
    private Date fechaActual; //obtiene la fecha del día actual
    private List<Emocion> emociones0; //emociones del día actual
    private List<Emocion> emociones1; //por cada día se asignará una lista de emociones de ese día
    private List<Emocion> emociones2;
    private List<Emocion> emociones3;
    private List<Emocion> emociones4;
    private List<Emocion> emociones5;
    private List<Emocion> emociones6;
    private List<Emocion> emociones7;
    private int contadorFeliz; //contadores de las apariciones de emociones
    private int contadorTriste ;
    private int contadorEnojado ;
    private int contadorAngustiado;
    private int contadorPreocupado;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadistica);

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        contadorFeliz = 0; //contadores de las apariciones de emociones, incializan en 0
        contadorTriste = 0;
        contadorEnojado = 0;
        contadorAngustiado = 0;
        contadorPreocupado = 0;

        mm = new manejadorMenu(EstadisticaActivity.this); // Se inicializa el manejador del menú,
        menu = findViewById(R.id.navViewEstad); // Se obtiene el menú por su id.
        menu.setNavigationItemSelectedListener(mm); // Se le asocia al menú su manejador.

        drawerLayout = findViewById(R.id.DrawerEstad);
        btn_menu = findViewById(R.id.btn_menu_estadistica);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        //implementa la funcionalidad del btn que dirige hacia la lista de temas
        btn_ListaTemas = findViewById(R.id.btn_estadistica_ListaTemas);
        btn_ListaTemas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EstadisticaActivity.this, ListaTemasActivity.class));
            }
        });

        imagenes = new int[]{R.drawable.imagen5, R.drawable.imagen6, R.drawable.imagen7,
                R.drawable.imagen8, R.drawable.imagen9};
        imagenEmocion = findViewById(R.id.elemento_estadistica_imagen);

        /*Si no carga la pantalla, puedes comentar desde la línea de abajo hasta donde asigna el texto.*/
        //inicializa la variable a mostrar, desde el intent
        nombreEmocionView = findViewById(R.id.txt_nombre_emocion);

        formatoFecha = new SimpleDateFormat("dd/MM/yyyy"); //inicializa el formato de fecha
        fechaActual = new Date(); //inicializa con la fecha de hoy
        fecha0 = formatoFecha.format(fechaActual); //se extrae la fecha del día actual

        base_datos = new DBHelper(getApplicationContext()); // Se inicializa el manejador de la base de datos.

        try { //obtiene las fechas de la semana pasada (-7 días a partir de hoy)
            obtenerFechasSemana();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //ahora que tiene las fechas de la semana pasada, solicita las emociones de cada día
        obtenerEmocionesFechasSemana();

        //ahora va a buscar por medio de contadores la emoción que aparece más, y extraerá su nombre
        String emocionFrecuenteSemana = emocion_frecuente_semana();
        nombreEmocionView.setText(emocionFrecuenteSemana); //se vincula con el texto del layout
        obtenerImagenEmocion(emocionFrecuenteSemana);
    }

    protected void obtenerImagenEmocion(String emocion) {
        if (emocion.equals("Triste")) {
            imagenEmocion.setImageResource(imagenes[0]);
        } else if (emocion.equals("Feliz")) {
            imagenEmocion.setImageResource(imagenes[1]);
        } else if (emocion.equals("Enojad@")) {
            imagenEmocion.setImageResource(imagenes[2]);
        } else if (emocion.equals("Angustiad@")) {
            imagenEmocion.setImageResource(imagenes[3]);
        } else if (emocion.equals("Preocupad@")) {
            imagenEmocion.setImageResource(imagenes[4]);
        }
    }

    protected void obtenerFechasSemana() throws ParseException {
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(fechaActual); //inicia el calendario desde la fecha actual
        //extra las fechas de una semana anterior a la fecha actual. Las pasa a string y las asigna al correspondiente
        calendario.add(Calendar.DATE, -1);
        Date ayer1 = calendario.getTime();
        fecha1 = formatoFecha.format(ayer1);
        calendario.add(Calendar.DATE, -1);
        Date ayer2 = calendario.getTime();
        fecha2 = formatoFecha.format(ayer2);
        calendario.add(Calendar.DATE, -1);
        Date ayer3 = calendario.getTime();
        fecha3 = formatoFecha.format(ayer3);
        calendario.add(Calendar.DATE, -1);
        Date ayer4 = calendario.getTime();
        fecha4 = formatoFecha.format(ayer4);
        calendario.add(Calendar.DATE, -1);
        Date ayer5 = calendario.getTime();
        fecha5 = formatoFecha.format(ayer5);
        calendario.add(Calendar.DATE, -1);
        Date ayer6 = calendario.getTime();
        fecha6 = formatoFecha.format(ayer6);
        calendario.add(Calendar.DATE, -1);
        Date ayer7 = calendario.getTime();
        fecha7 = formatoFecha.format(ayer7);
    }

    protected void obtenerEmocionesFechasSemana(){
        // Se obtienen las emociones de una correspondiente fecha.
        emociones0 = base_datos.obtenerEmocionesFecha(fecha0);
        emociones1 = base_datos.obtenerEmocionesFecha(fecha1);
        emociones2 = base_datos.obtenerEmocionesFecha(fecha2);
        emociones3 = base_datos.obtenerEmocionesFecha(fecha3);
        emociones4 = base_datos.obtenerEmocionesFecha(fecha4);
        emociones5 = base_datos.obtenerEmocionesFecha(fecha5);
        emociones6 = base_datos.obtenerEmocionesFecha(fecha6);
        emociones7 = base_datos.obtenerEmocionesFecha(fecha7);
    }

    protected String emocion_frecuente_semana() {
        //recorre cada lista verificando cuántas veces aparece una emoción
        if (emociones1.isEmpty() || emociones2.isEmpty() || emociones3.isEmpty() ||
            emociones4.isEmpty() || emociones5.isEmpty() || emociones6.isEmpty() || emociones7.isEmpty()) {
            //si las emociones de la semana no están completas, no se puede extraer más que del día actual
            buscaEmocionesLista(emociones0);
        } else { //si hay emociones en todos los días
            buscaEmocionesLista(emociones0);
            buscaEmocionesLista(emociones1);
            buscaEmocionesLista(emociones2);
            buscaEmocionesLista(emociones3);
            buscaEmocionesLista(emociones4);
            buscaEmocionesLista(emociones5);
            buscaEmocionesLista(emociones6);
            buscaEmocionesLista(emociones7);
        }
        //busca la emoción con el contador más alto, es decir, que más se repitió
        int[] contadoresEmociones = {contadorFeliz, contadorTriste, contadorEnojado, contadorAngustiado, contadorPreocupado};
        int indiceEmocionMasFrecuente = 0;
        for (int posicion = 0; posicion < contadoresEmociones.length; posicion++) {
            if (contadoresEmociones[posicion]> contadoresEmociones[indiceEmocionMasFrecuente]) {
                indiceEmocionMasFrecuente = posicion; //almacena el índice de la casilla con el número más alto, la emoción más común
            }
        }
        return verificaNombreEmocion(indiceEmocionMasFrecuente); //regresa el nombre de la emocion, por medio de su índice
    }

    protected String verificaNombreEmocion(int indiceEmocion) {
        if (indiceEmocion==0) {
            return "Feliz";
        } else if (indiceEmocion==1) {
            return "Triste";
        } else if (indiceEmocion==2) {
            return "Enojad@";
        } else if (indiceEmocion==3) {
            return "Angustiad@";
        } else if (indiceEmocion==4) {
            return "Preocupad@";
        }
        return null;
    }

    protected void buscaEmocionesLista(List<Emocion> listaEmociones){
        for(int indice = 0; indice < listaEmociones.size(); indice++) {
            if (verificaEmocionFeliz(listaEmociones.get(indice).getNombre())) {
                contadorFeliz++;
            } else if (verificaEmocionTriste(listaEmociones.get(indice).getNombre())) {
                contadorTriste++;
            } else if (verificaEmocionEnojado(listaEmociones.get(indice).getNombre())) {
                contadorEnojado++;
            } else if (verificaEmocionAngustiado(listaEmociones.get(indice).getNombre())) {
                contadorAngustiado++;
            } else if (verificaEmocionPreocupado(listaEmociones.get(indice).getNombre())) {
                contadorPreocupado++;
            }
        }
    }

    protected boolean verificaEmocionFeliz(String emocion) {
        if (emocion.equals("Feliz")){
            return true;
        } else {
            return false;
        }
    }

    protected boolean verificaEmocionTriste(String emocion) {
        if (emocion.equals("Triste")){
            return true;
        } else {
            return false;
        }
    }

    protected boolean verificaEmocionEnojado(String emocion) {
        if (emocion.equals("Enojad@")){
            return true;
        } else {
            return false;
        }
    }

    protected boolean verificaEmocionAngustiado(String emocion) {
        if (emocion.equals("Angustiad@")){
            return true;
        } else {
            return false;
        }
    }

    protected boolean verificaEmocionPreocupado(String emocion) {
        if (emocion.equals("Preocupad@")){
            return true;
        } else {
            return false;
        }
    }
}