package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;

public class ListAdapter extends ArrayAdapter<Tema> {

    public ListAdapter(Context context, ArrayList<Tema> temaArrayList) {
        super(context, R.layout.elemento_lista_tema, temaArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Tema tema = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.elemento_lista_tema, parent, false);
        }

        ImageView imageView = convertView.findViewById(R.id.elemento_tema_imagen);
        TextView titulo = convertView.findViewById(R.id.elemento_tema_name);

        imageView.setImageResource(tema.imagenId);
        titulo.setText(tema.titulo);

        return convertView;
    }
}
