package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

import androidx.annotation.NonNull;

public class Emocion {
    private int id;
    private String nombre;

    public Emocion(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @NonNull
    @Override
    public String toString() {
        return nombre;
    }
}
