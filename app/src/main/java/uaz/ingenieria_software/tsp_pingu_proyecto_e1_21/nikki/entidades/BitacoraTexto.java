package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class BitacoraTexto extends Bitacora {

    private static final long serialVersionUID = 1L;

    public BitacoraTexto(int identificador, String texto_bitacora, String fecha_bitacora){
        super(identificador, texto_bitacora, fecha_bitacora);
    }
    public BitacoraTexto(int identificador,String titulo_bitacora, String texto_bitacora, String fecha_bitacora,int emocion){
        super(identificador,titulo_bitacora, texto_bitacora, fecha_bitacora,emocion);
    }

    public int getId() {
        return id;
    }

    public String getFecha() {
        return fecha;
    }

    public String getBitacora() {
        return info;
    }

    public int getEmocion() {
        return emocion;
    }

    @NonNull
    @Override
    public String toString() {
        return "BitacoraTexto_"+ id + " " + fecha;
    }
}
