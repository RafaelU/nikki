package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Emocion;

public class BitacoraTextoActivity extends AppCompatActivity {
    // Atributos necesarios de la clase
    private NavigationView menu;
    private manejadorMenu mm;
    private View btn_atras;
    private View btn_guardar;
    private EditText texto_bitacora_titulo;
    private Spinner spinerEmocion;
    private ArrayAdapter<Emocion> datosSpiner;
    private EditText texto_bitacora;
    private DBHelper dbhelper;
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;

    protected void onCreate(Bundle savedInstanceState) {
        //Se crea la instancia de la actividad.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitacora_texto);

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Se busca el menú de activity_bitacora_texto.xml.
        // Se crea una instancia de la clase manejadorMenu y se le asigna al menú.
        menu = findViewById(R.id.navViewBitacoraTexto);
        mm = new manejadorMenu(BitacoraTextoActivity.this);
        menu.setNavigationItemSelectedListener(mm);

        drawerLayout = findViewById(R.id.DrawerBitTxt);
        btn_menu = findViewById(R.id.btn_menu_BitTxt);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        // Se busca el botón atras de activity_bitacora_texto.xml.
        // Se crea una instancia de la clase manejadorAtrasBtnBitacoraTexto y se le asigna a el botón.
        btn_atras = findViewById(R.id.btn_Atras_Bitacora_texto);
        manejadorAtrasBtnBitacoraTexto mabt = new manejadorAtrasBtnBitacoraTexto();
        btn_atras.setOnClickListener(mabt);

        // Se busca el edittext de activity_bitacora_texto.xml.
        // Se busca el botón de guardar de activity_bitacora_texto.xml.
        texto_bitacora = findViewById(R.id.txt_Bitacora);
        texto_bitacora_titulo = findViewById(R.id.editTextAgregarTiuloBitacoraTexto);
        btn_guardar = findViewById(R.id.btn_Guardar_Bitacora_Texto);
        manejadorGuardarBtnBitacoraTexto mgbbt = new manejadorGuardarBtnBitacoraTexto();
        btn_guardar.setOnClickListener(mgbbt);

        // Se crea una instancia de la clase DBHelper para hacer las inserciones en la base de datos.
        dbhelper = new DBHelper(getApplicationContext());


        List<Emocion> e = dbhelper.obtenerEmociones();
        datosSpiner = new ArrayAdapter<Emocion>(this,android.R.layout.simple_spinner_item,e);
        spinerEmocion = findViewById(R.id.spinnerEmociónBitacoraTexto);
        spinerEmocion.setAdapter(datosSpiner);
        spinerEmocion.setSelection(0);

    }
    // Clase perteneciente a la funcionalidad del botón atrás
    private class manejadorAtrasBtnBitacoraTexto implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            regresar(v);
        }
    }

    //Método para terminar la activity y regresar
    public void regresar(View v){
        finish();
    }

    @Override
    public void finish() {

        super.finish();
    }

    // Clase que guarda las bitácoras de texto en la base de datos
    private class manejadorGuardarBtnBitacoraTexto implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            // Verifica si se escribió algo en el EditText, si no es así, muestra un mensaje.
            if (!texto_bitacora.getText().toString().isEmpty() && !texto_bitacora_titulo.getText().toString().isEmpty()) {

                // Se estrae la fecha actual.
                long ahora = System.currentTimeMillis();
                Date fecha = new Date(ahora);
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String salida = df.format(fecha);
                Emocion e = (Emocion)spinerEmocion.getSelectedItem();
                // Se agrega el registro a la base de datos.
                dbhelper.añadirNuevaBitacoraTXT(texto_bitacora_titulo.getText().toString(),texto_bitacora.getText().toString(), salida,e.getId() );

                // Se muestra un mensaje de registro exitoso.
                Toast.makeText(getApplicationContext(), R.string.msg_registro_exitoso_bitacora_texto, Toast.LENGTH_SHORT).show();

                // Se regresa al activity principal (MainActivity).
                Intent intent = new Intent(BitacoraTextoActivity.this, MainActivity.class);
                startActivity(intent);
            }
            else{
                Toast.makeText(getApplicationContext(),R.string.msg_no_hay_texto, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
