package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Bitacora;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraAudio;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.BitacoraTexto;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Emocion;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "Nikki_Registros";
    private static final String EMOCION_TABLE = "EMOCION";
    private static final String ID_EMOCION = "ID_EMOCION";
    private static final String EMOCION_DESC = "EMOCION_DESC";

    private static final String BITACORATXT_TABLE = "BITACORA_TEXTO";
    private static final String TITULO_BITACTXT = "TITULO";
    private static final String ID_BITACORATXT = "ID_BITACORATXT";
    private static final String BITACORA_TEXTO = "BITACORA_TEXTO";
    private static final String FECHA_BITACTXT = "FECHA";

    private static final String BITACORAAUD_TABLE = "BITACORA_AUDIO";
    private static final String TITULO_BITACAUD = "TITULO";
    private static final String ID_BITACORAAUD = "ID_BITACORAAUD";
    private static final String BITACORA_AUD_PATH = "BITACORA_AUD_PATH";
    private static final String FECHA_BITACAUD = "FECHA";

    // Constructor
    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    // Este método es para crear la base de datos usando sqlite query
    @Override
    public void onCreate(SQLiteDatabase db) {
        String tabla_emocion = "CREATE TABLE " + EMOCION_TABLE + "("
                + ID_EMOCION + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EMOCION_DESC + " TEXT NOT NULL);";
        System.out.println(tabla_emocion);

        String tabla_bitacoratxt = "CREATE TABLE " + BITACORATXT_TABLE + "("
                + ID_BITACORATXT + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TITULO_BITACTXT + " TEXT NOT NULL, "
                + BITACORA_TEXTO + " TEXT NOT NULL, "
                + FECHA_BITACTXT + " TEXT NOT NULL, "
                + ID_EMOCION + "  INTEGER NOT NULL, "
                + "FOREIGN KEY (" + ID_EMOCION + ") REFERENCES " + EMOCION_TABLE + "("+ ID_EMOCION +"));";

        String tabla_bitacoraaud = "CREATE TABLE " + BITACORAAUD_TABLE + "("
                + ID_BITACORAAUD + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TITULO_BITACAUD + " TEXT NOT NULL, "
                + BITACORA_AUD_PATH + " TEXT, "
                + FECHA_BITACAUD + " TEXT NOT NULL, "
                + ID_EMOCION + "  INTEGER NOT NULL, "
                + "FOREIGN KEY (" + ID_EMOCION + ") REFERENCES " + EMOCION_TABLE + "("+ ID_EMOCION +"));";

        db.execSQL(tabla_emocion);
        System.out.println();
        db.execSQL("INSERT INTO EMOCION(EMOCION_DESC) VALUES('Feliz');");
        db.execSQL("INSERT INTO EMOCION(EMOCION_DESC) VALUES('Triste');");
        db.execSQL("INSERT INTO EMOCION(EMOCION_DESC) VALUES('Enojad@');");
        db.execSQL("INSERT INTO EMOCION(EMOCION_DESC) VALUES('Angustiad@');");
        db.execSQL("INSERT INTO EMOCION(EMOCION_DESC) VALUES('Preocupad@');");

        db.execSQL(tabla_bitacoratxt);
        db.execSQL(tabla_bitacoraaud);

/*
        String[] temas = DBTemas.temas;
        for (int ind=0; ind<temas.length; ind++){
            db.execSQL(temas[ind]);
        }

 */
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void añadirNuevaBitacoraTXT(String titulo,String bitacora, String id_dia, int emocion){
        SQLiteDatabase db = this.getReadableDatabase();
        if (db!=null){
            db.execSQL("INSERT INTO BITACORA_TEXTO(TITULO,BITACORA_TEXTO,FECHA,ID_EMOCION) VALUES('"+titulo+"','"+bitacora+"','"+id_dia+"','"+emocion+"');");
            db.close();
        }
    }

    public void añadirNuevaBitacoraAUD(BitacoraAudio bitacoraAUD){
        SQLiteDatabase db = this.getReadableDatabase();
        if (db!=null){
            db.execSQL("INSERT INTO BITACORA_AUDIO(TITULO,BITACORA_AUD_PATH,FECHA,ID_EMOCION) VALUES('"+bitacoraAUD.getTitulo()+"','"+bitacoraAUD.getPath()+"','"+bitacoraAUD.getFecha()+"','"+bitacoraAUD.getEmocion()+"');");
            db.close();
        }

    }

    public boolean eliminarBitacoraTXT(int id){
        boolean eliminado = false;
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.execSQL("DELETE FROM " + BITACORATXT_TABLE + " WHERE "+ ID_BITACORATXT +" = '" + id + "'");
            eliminado = true;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            eliminado = false;
        }
        finally {
            db.close();
        }
        return eliminado;
    }

    public boolean eliminarBitacoraAUD(int id){
        boolean eliminado = false;
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.execSQL("DELETE FROM " + BITACORAAUD_TABLE + " WHERE "+ ID_BITACORAAUD +" = '" + id + "'");
            eliminado = true;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            eliminado = false;
        }
        finally {
            db.close();
        }
        return eliminado;
    }

    public List<Emocion> obtenerEmociones(){
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT * FROM "+EMOCION_TABLE,null);
        List<Emocion> emocion_lista= new ArrayList<>();
        if(cursor.moveToFirst()){
            do{
                emocion_lista.add(new Emocion(Integer.parseInt(cursor.getString(0)),cursor.getString(1)));
            }while (cursor.moveToNext());
        }
        bd.close();
        return emocion_lista;

    }

    public Emocion obtenerEmocion(int id_emocion){

        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT EMOCION_DESC FROM " + EMOCION_TABLE + " WHERE "+ID_EMOCION+" = "+ id_emocion,null);

        if(cursor.moveToFirst()){
            Emocion e = new Emocion(id_emocion,cursor.getString(0));
            return e;
        }
        return null;
    }

    public List<Bitacora> obtenerBitacoras(){

        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT * FROM BITACORA_TEXTO",null);
        List<Bitacora> bitacora_lista= new ArrayList<>();
        if(cursor.moveToFirst()){
            do{
                bitacora_lista.add(new BitacoraTexto(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),Integer.parseInt(cursor.getString(4))));
            }while (cursor.moveToNext());
        }
        cursor.close();
        cursor = bd.rawQuery("SELECT * FROM BITACORA_AUDIO",null);
        if(cursor.moveToFirst()){
            do{
                bitacora_lista.add(new BitacoraAudio(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),Integer.parseInt(cursor.getString(4))));
            }while (cursor.moveToNext());
        }
        cursor.close();
        bd.close();
        return bitacora_lista;
    }

    public List<Emocion> obtenerEmocionesFecha(String fecha) {
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT ID_EMOCION FROM BITACORA_TEXTO WHERE FECHA='"+fecha+"'",null);
        List<Emocion> emociones_lista= new ArrayList<>();
        if(cursor.moveToFirst()){
            do{ //cada vez que se mueva al siguiente cursor, extrae de nuevo los datos de esa emoción
                Emocion e_txt = obtenerEmocion(Integer.parseInt(cursor.getString(0)));
                emociones_lista.add(e_txt); //almacena las bitacoras
            }while (cursor.moveToNext());
        }
        Cursor cursor_aud = bd.rawQuery("SELECT * FROM BITACORA_AUDIO WHERE FECHA='"+fecha+"'",null);
        if(cursor_aud.moveToFirst()){
            do{
                Emocion e_aud = obtenerEmocion(Integer.parseInt(cursor_aud.getString(0)));
                emociones_lista.add(e_aud);
            }while (cursor_aud.moveToNext());
        }
        bd.close();
        return emociones_lista;
    }
}
