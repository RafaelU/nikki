package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Date;

public class Bitacora implements Serializable {
    protected int id;
    protected String titulo;
    protected String fecha;
    protected String info;
    protected int emocion;

    public Bitacora(int id, String info, String fecha) {
        this.id=id;
        this.fecha = fecha;
        this.info = info;
    }
    public Bitacora(int id,String titulo, String info, String fecha,int emocion) {
        this.id=id;
        this.titulo=titulo;
        this.emocion=emocion;
        this.fecha = fecha;
        this.info = info;
    }
    public Bitacora(String fecha) {
        this.fecha = fecha;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getFecha() {
        return fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getEmocion() {
        return emocion;
    }

    public void setEmocion(int emocion) {
        this.emocion = emocion;
    }



    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @NonNull
    @Override
    public String toString() {
        return "BitacoraObject_" + id + " " + fecha;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj instanceof Bitacora){
            if (this.id == ((Bitacora) obj).id && this.fecha.equals(((Bitacora) obj).fecha)){
                return true;
            }else {
                return false;
            }
        }else {
            return  false;
        }
    }
}
