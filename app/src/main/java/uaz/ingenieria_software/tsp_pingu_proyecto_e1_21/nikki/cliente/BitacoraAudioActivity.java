package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.util.List;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase.DBHelper;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.entidades.Emocion;

public class BitacoraAudioActivity extends AppCompatActivity {
    private TextView btnRegresar;
    private TextView txtComentarios;
    private TextView txtIndicacionGrabacion;
    private EditText texto_bitacora_titulo;
    private MaterialButton btnGrabar;
    private File path;
    private DBHelper dbhelper;
    private NavigationView menu;
    private manejadorMenu mm;
    private MaterialButton btn_menu;
    private DrawerLayout drawerLayout;
    btnGrabacionManejador manejadorBtnGrabar;

    private Spinner spinerEmocion;
    private ArrayAdapter<Emocion> datosSpiner;


    private boolean grabar = true;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitacora_audio);

        //Evita la rotación de la pantalla
        getRequestedOrientation();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Se crea una instancia de manejadorMenu y se asocia al menú.
        mm = new manejadorMenu(BitacoraAudioActivity.this);
        menu = findViewById(R.id.navViewBitacoraAudio);
        menu.setNavigationItemSelectedListener(mm);

        drawerLayout = findViewById(R.id.DrawerBitAud);
        btn_menu = findViewById(R.id.btn_menu_BitAud);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        //se inicializan las variables de los widgets
        btnRegresar = findViewById(R.id.textAtrasGrabacion);
        texto_bitacora_titulo = findViewById(R.id.editTextAgregarTiuloBitacoraAud);
        txtComentarios = findViewById(R.id.textComentariosGrabar);
        txtIndicacionGrabacion = findViewById(R.id.textIndicacionesGrabar);
        btnGrabar = findViewById(R.id.buttonGrabacion);

        //se colocan los estados iniciales de los widgets
        setWidgetsNoGrabando();

        manejadorBtnGrabar = new btnGrabacionManejador();
        btnGrabar.setOnClickListener(manejadorBtnGrabar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regresar(view);
            }
        });

        //se crea el directorio de las grabaciones
        path = new File(getExternalFilesDir(null)+"/sonido");
        if (path.exists() == false){
            path.mkdir();
        }

        //se cargan las emociones
        dbhelper = new DBHelper(getApplicationContext());
        List<Emocion> e = dbhelper.obtenerEmociones();
        datosSpiner = new ArrayAdapter<Emocion>(this,android.R.layout.simple_spinner_item,e);
        spinerEmocion = findViewById(R.id.spinnerEmociónBitacoraAud);
        spinerEmocion.setAdapter(datosSpiner);
        spinerEmocion.setSelection(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @SuppressLint("ResourceAsColor")
    private void setWidgetsNoGrabando(){
        btnRegresar.setVisibility(View.VISIBLE);
        btn_menu.setVisibility(View.VISIBLE);
        menu.setNavigationItemSelectedListener(mm);

        btnGrabar.setIconTintResource(R.color.white);
        btnGrabar.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this,R.color.crema)));
        btnGrabar.setOutlineAmbientShadowColor(R.color.crema);
        btnGrabar.setOutlineSpotShadowColor(R.color.crema);
        btnGrabar.setRippleColor(ColorStateList.valueOf(R.color.crema));
        btnGrabar.setOutlineAmbientShadowColor(R.color.crema);
        btnGrabar.setStrokeColor(ColorStateList.valueOf(R.color.crema));
        btnGrabar.setHintTextColor(R.color.crema);

        txtComentarios.setTextColor(ContextCompat.getColor(this,R.color.amarillo));
        txtComentarios.setText(R.string.Saludo2);

        txtIndicacionGrabacion.setTextColor(ContextCompat.getColor(this,R.color.amarillo));
        txtIndicacionGrabacion.setText(R.string.grabacion_iniciar);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @SuppressLint("ResourceAsColor")
    private void setWidgetsGrabando(){
        btnRegresar.setVisibility(View.GONE);
        btn_menu.setVisibility(View.GONE);
        menu.setNavigationItemSelectedListener(null);

        btnGrabar.setIconTintResource(R.color.crema);
        btnGrabar.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this,R.color.white)));
        btnGrabar.setOutlineAmbientShadowColor(R.color.crema);
        btnGrabar.setOutlineSpotShadowColor(R.color.crema);
        btnGrabar.setRippleColor(ColorStateList.valueOf(R.color.crema));
        btnGrabar.setOutlineAmbientShadowColor(R.color.crema);
        btnGrabar.setStrokeColor(ColorStateList.valueOf(R.color.crema));
        btnGrabar.setHintTextColor(R.color.crema);

        txtComentarios.setTextColor(ContextCompat.getColor(this,R.color.naranja_claro));
        txtComentarios.setText(R.string.Saludo3Resp);

        txtIndicacionGrabacion.setTextColor(ContextCompat.getColor(this,R.color.naranja_claro));
        txtIndicacionGrabacion.setText(R.string.grabacion_parar);
    }

    private class btnGrabacionManejador implements View.OnClickListener{
        Intent intent = new Intent(BitacoraAudioActivity.this, Grabador.class);

        @RequiresApi(api = Build.VERSION_CODES.P)
        @Override
        public void onClick(View view) {
            if (!texto_bitacora_titulo.getText().toString().isEmpty()){
                intent.putExtra("titulo",texto_bitacora_titulo.getText().toString());
                intent.putExtra("idEmocion",((Emocion)spinerEmocion.getSelectedItem()).getId());
                grabar = !grabar;
                if(grabar == false){
                    texto_bitacora_titulo.setFocusable(false);
                    texto_bitacora_titulo.setEnabled(false);
                    setWidgetsGrabando();
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    startService(intent);

                }else {
                    setWidgetsNoGrabando();
                    stopService(intent);
                    texto_bitacora_titulo.setFocusable(true);
                    texto_bitacora_titulo.setEnabled(true);
                }
            }
            else{
                Toast.makeText(getApplicationContext(),R.string.msg_no_hay_texto, Toast.LENGTH_SHORT).show();
            }

        }

        public Intent getIntent(){
            return intent;
        }
    }



    //Método para terminar la activity y regresar
    public void regresar(View v){
        finish();
    }

    private void saliendo(){
        if(grabar == false){
            Intent intent = manejadorBtnGrabar.getIntent();
            stopService(intent);
        }
    }

    @Override
    public void finish() {
        saliendo();
        super.finish();
    }

    @Override
    protected void onPause() {
        saliendo();
        super.onPause();
    }

    @Override
    protected void onStop() {
        saliendo();
        super.onStop();
    }

    @Override
    protected void onUserLeaveHint() {
        saliendo();
        super.onUserLeaveHint();
    }

    @Override
    public void onBackPressed() {
        saliendo();
        super.onBackPressed();
    }
}