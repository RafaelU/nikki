package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.DataBase;

public class DBTemas {
    public static final String[] temas = {
            "INSERT INTO TEMA(TEMA_TITULO,TEMA_CONTENIDO,TEMA_IMG_PATH) VALUES(" +
                    "'¿Qué es la Ansiedad Generalizada?'," +
                    "'Es un trastorno mental en el cual una persona a menudo está preocupada o ansiosa respecto a muchas cosas y le parece difícil controlar esta ansiedad.'," +
                    "'/recursosTemas/tema1.jpg'" +
                    ")",

            "INSERT INTO TEMA(TEMA_TITULO,TEMA_CONTENIDO,TEMA_IMG_PATH) VALUES(" +
                    "'Factores de riesgo'," +
                    "'Personalidad. Una persona que es tímida o que tiene un temperamento negativo o que evita cualquier situación peligrosa puede ser más propensa a padecer un trastorno de ansiedad generalizada que otras.'," +
                    "'/recursosTemas/tema2.jpg'" +
                    ")",

            "INSERT INTO TEMA(TEMA_TITULO,TEMA_CONTENIDO,TEMA_IMG_PATH) VALUES(" +
                    "'Síntomas'," +
                    "'Preocupación o ansiedad persistentes por determinados asuntos que son desproporcionados en relación con el impacto de los acontecimientos \n Pensar demasiado los planes y las soluciones a todos los peores resultados posibles \n Incapacidad para relajarse, sensación de nerviosismo y sensación de excitación o de estar al límite.'," +
                    "'/recursosTemas/tema2.jpg'" +
                    ")",

            "INSERT INTO TEMA(TEMA_TITULO,TEMA_CONTENIDO,TEMA_IMG_PATH) VALUES(" +
                    "'Causas'," +
                    "'Como sucede con muchas enfermedades mentales, la causa del trastorno de ansiedad generalizada probablemente surge de una interacción compleja de factores biológicos y ambientales.'," +
                    "'/recursosTemas/tema2.jpg'" +
                    ")",
    };

    public static final String[] recursosImg = {
            "tema1.jpg",
            "tema2.jpg"
    };
}
