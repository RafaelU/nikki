package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;


public class RecordatorioBroadcast extends BroadcastReceiver {
    @Override
    /**
     * Metodo que se encarga de mandar llamar al ayudador de notificaciones.
     * Así como que se encarga de seleccionar qué notificación se le mostrará al usuario dependiendo de la hora actual.
     * @param context contexto
     * @param intent intent
     */
    public void onReceive(Context context, Intent intent) {
        AyudadorNotificacion ayudadorNotificacion = new AyudadorNotificacion(context);

        Calendar ahora = Calendar.getInstance();
        int currentTimein24Format = ahora.get(Calendar.HOUR_OF_DAY); //Obtenemos la hora actual
        int idNotif = 0;
        //Dependiendo que hora sea, arrojará el mensaje correspondiente
        if(currentTimein24Format >= 15 && currentTimein24Format <19){ //tarde
            idNotif = 0;
        }
        else if(currentTimein24Format >= 19 && currentTimein24Format <= 24){ //noche
            idNotif = 1;
        }
        //Se manda llamar el metodo que creará la notificación pasándole el id para mostrar el mensaje correspondiente
        ayudadorNotificacion.crearNotificacion(idNotif);
    }
}
