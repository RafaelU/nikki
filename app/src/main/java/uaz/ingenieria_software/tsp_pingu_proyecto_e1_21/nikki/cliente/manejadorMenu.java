package uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;

import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.R;
import uaz.ingenieria_software.tsp_pingu_proyecto_e1_21.nikki.cliente.MainActivity;

// Clase que se encarga de manejar el menú lateral.
public class manejadorMenu implements NavigationView.OnNavigationItemSelectedListener {

    // Atributos necesarios.
    private Context act;

    // Constructor de la clase el cual recibe el contexto de la actividad en la que se encuentra.
    public manejadorMenu(Context actividad){
        act= actividad;
    }

    // Método que se encarga del funcionamiento del menú lateral.
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==R.id.menu_bitacora){
            Intent intent_bitacora = new Intent(act, MainActivity.class);
            act.startActivity(intent_bitacora);
        }
        else if (item.getItemId()==R.id.menu_temas){
            Intent intent_temas = new Intent(act, ListaTemasActivity.class);
            act.startActivity(intent_temas);
        }
        else if (item.getItemId()==R.id.menu_registros){
            Intent intent_registros = new Intent(act, ListaBitacorasActivity.class);
            act.startActivity(intent_registros);
        }
        else if (item.getItemId()==R.id.menu_estadisticas){
            Intent intent_estadisticas = new Intent(act, EstadisticaActivity.class);
            act.startActivity(intent_estadisticas);
        }
        return false;
    }
}
